REM Navigation, Logo and Slider
ngdart generate component Airisshane --path lib/src/Material/Navigation/airisshane
ngdart generate component Zestia --path lib/src/Material/Logo/zestia
ngdart generate component Meldin --path lib/src/Material/Slider/meldin
REM Carousel
ngdart generate component Meldin --path lib/src/Material/Slider/meldin
REM All other homepage section and Elements.
REM Such as: About, Services, Portfolio, Counter, Team, Testimonial, Client Logo, Blog, Pricing, and Contact Us
ngdart generate component Wroognygothor --path lib/src/Material/About/Wroognygothor
ngdart generate component Ackmard --path lib/src/Material/Services/Ackmard
ngdart generate component Gametris --path lib/src/Material/Portfolio/Gametris
ngdart generate component Fhuyraslan --path lib/src/Material/Counter/Fhuyraslan
ngdart generate component Zigmal --path lib/src/Material/Team/Zigmal
ngdart generate component Gamud --path lib/src/Material/Testimonial/Gamud
ngdart generate component Aldarenupdar --path lib/src/Material/Client/Aldarenupdar
ngdart generate component Kressara --path lib/src/Material/Logo/Kressara
ngdart generate component Xeniljulthor --path lib/src/Material/Blog/Xeniljulthor
ngdart generate component Hecton --path lib/src/Material/Pricing/Hecton
ngdart generate component Kaldar --path lib/src/Material/ContactUs/Kaldar
ngdart generate component Teressa --path lib/src/Material/p404/Teressa
ngdart generate component DaigornSabalz --path lib/src/Material/p422/DaigornSabalz
ngdart generate component Onathe --path lib/src/Material/p500/Onathe

Rem Components Shared Parts
ngdart generate component Errinaya --path lib/src/Material/shared/Header/Errinaya
ngdart generate component Mehanderyodan --path lib/src/Material/shared/Carousel/Mehanderyodan
ngdart generate component BreenElthen --path lib/src/Material/shared/About/BreenElthen
ngdart generate component Talberonderik --path lib/src/Material/shared/Welcome/Talberonderik
ngdart generate component Othelenyerpal --path lib/src/Material/shared/Services/Othelenyerpal
ngdart generate component ModricNaphazw --path lib/src/Material/shared/Portfolio/ModricNaphazw
ngdart generate component Ahanna --path lib/src/Material/shared/WorkCounter/Ahanna
ngdart generate component Abardonrelban --path lib/src/Material/shared/Team/Abardonrelban
ngdart generate component Senic --path lib/src/Material/shared/Team/Senic
ngdart generate component Akara --path lib/src/Material/shared/Testimonial/Akara
ngdart generate component KassinaTristan --path lib/src/Material/shared/Client/KassinaTristan
ngdart generate component ValtaurGreste --path lib/src/Material/shared/BLog/ValtaurGreste
ngdart generate component Ayne --path lib/src/Material/shared/ContactUs/Ayne
ngdart generate component Teressa --path lib/src/Material/shared/Action/Teressa
ngdart generate component Laenaya --path lib/src/Material/shared/Accirdion/Laenaya
ngdart generate component Luna --path lib/src/Material/shared/Alert/Luna
ngdart generate component Urda --path lib/src/Material/shared/Pricing/Urda
ngdart generate component Rahe --path lib/src/Material/shared/Pricing/Rahe
ngdart generate component Irina --path lib/src/Material/shared/Pricing/Irina
ngdart generate component Useli --path lib/src/Material/shared/Tabs/Useli
ngdart generate component Qupar --path lib/src/Material/shared/Tabs/Qupar
ngdart generate component Vilan --path lib/src/Material/shared/Tabs/Vilan


REM Pages
ngdart generate component About --path lib/src/Material/pages/About
ngdart generate component AddProduct --path lib/src/Material/pages/AddProduct
ngdart generate component BlogPost --path lib/src/Material/pages/BlogPost
ngdart generate component BlogPosts --path lib/src/Material/pages/BlogPosts
ngdart generate component ContactUs --path lib/src/Material/pages/ContactUs
ngdart generate component Discover --path lib/src/Material/pages/Discover
ngdart generate component Ecommerce --path lib/src/Material/pages/Ecommerce
ngdart generate component Home --path lib/src/Material/pages/Home
ngdart generate component Landing --path lib/src/Material/pages/Landing
ngdart generate component Licenses --path lib/src/Material/pages/Licenses
ngdart generate component Login --path lib/src/Material/pages/Login
ngdart generate component P404 --path lib/src/Material/pages/P404
ngdart generate component P422 --path lib/src/Material/pages/P422
ngdart generate component P500 --path lib/src/Material/pages/P500
ngdart generate component Product --path lib/src/Material/pages/Product
ngdart generate component Profile --path lib/src/Material/pages/Profile
ngdart generate component Register --path lib/src/Material/pages/Register
ngdart generate component SearchSidebar --path lib/src/Material/pages/SearchSidebar
ngdart generate component Settings --path lib/src/Material/pages/Settings
ngdart generate component TeamInfo --path lib/src/Material/pages/TeamInfo