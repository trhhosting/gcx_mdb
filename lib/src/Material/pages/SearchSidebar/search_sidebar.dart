import 'package:angular/angular.dart';

import '../../shared/About/BreenElthen/breen_elthen.dart';
import '../../shared/Accirdion/Laenaya/laenaya.dart';
import '../../shared/Action/Teressa/teressa.dart';
import '../../shared/Alert/Luna/luna.dart';
import '../../shared/BLog/ValtaurGreste/valtaur_greste.dart';
import '../../shared/Carousel/Mehanderyodan/mehanderyodan.dart';
import '../../shared/Client/KassinaTristan/kassina_tristan.dart';
import '../../shared/ContactUs/Ayne/ayne.dart';
import '../../shared/Header/Errinaya/errinaya.dart';
import '../../shared/Portfolio/ModricNaphazw/modric_naphazw.dart';
import '../../shared/Pricing/Irina/irina.dart';
import '../../shared/Pricing/Rahe/rahe.dart';
import '../../shared/Pricing/Urda/urda.dart';
import '../../shared/Services/Othelenyerpal/othelenyerpal.dart';
import '../../shared/Tabs/Qupar/qupar.dart';
import '../../shared/Tabs/Useli/useli.dart';
import '../../shared/Tabs/Vilan/vilan.dart';
import '../../shared/Team/Abardonrelban/abardonrelban.dart';
import '../../shared/Team/Senic/senic.dart';
import '../../shared/Testimonial/Akara/akara.dart';
import '../../shared/Welcome/Talberonderik/talberonderik.dart';
import '../../shared/WorkCounter/Ahanna/ahanna.dart';

@Component(
  selector: 'search-sidebar',
  templateUrl: 'search_sidebar.html',
  directives: [
    coreDirectives,
    Errinaya,
    Mehanderyodan,
    BreenElthen,
    Talberonderik,
    Othelenyerpal,
    ModricNaphazw,
    Ahanna,
    Abardonrelban,
    Senic,
    Akara,
    KassinaTristan,
    ValtaurGreste,
    Ayne,
    Teressa,
    Laenaya,
    Luna,
    Urda,
    Rahe,
    Irina,
    Useli,
    Qupar,
    Vilan,
  ],
)
class SearchSidebar {
  var name = 'Angular';
}
