import 'package:angular/angular.dart';

@Component(
  selector: 'teressa',
  templateUrl: 'teressa.html',
)
class Teressa {
  String errorCode = "404";
  String errorMsg = "Page not found! :(";
}
