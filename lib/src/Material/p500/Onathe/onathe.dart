import 'package:angular/angular.dart';

@Component(
  selector: 'onathe',
  templateUrl: 'onathe.html',
)
class Onathe {
  String errorCode = "500";
  String errorMsg = "Opps something went wrong try again.";
}
