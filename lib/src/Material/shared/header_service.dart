import 'dart:async';

import 'package:app/logic/header.dart';

import 'headerMock.dart';

class HeaderService {
  Future<List<HeaderElthen>> getHeaderElthen() async => loadHeaderElthen();
  Future<List<HeaderNadorn>> getHeaderNadorn() async => loadHeaderNadorn();
  Future<List<HeaderMehanderyodan>> getHeaderMehanderyodan() async => loadHeaderMehanderyodan();
  Future<List<HeaderZigmal>> getHeaderZigmal() async => loadHeaderZigmal();
}
