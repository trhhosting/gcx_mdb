import 'dart:math';

import 'package:angular/angular.dart';

@Component(
  selector: 'teressa',
  templateUrl: 'teressa.html',
)
class Teressa implements OnInit {
  Random num = new Random();

  String Title = 'Call us @800-555-5555';
  String SubTitle = "Enjoy our 24/7 support";
  String btnMsg = "Contact Us";
  String photo; // = "/static/images/mobile/tlm-c${num.nextInt(70)}1024by720.jpg"
  @override
  void ngOnInit() {
    this.photo = "/static/images/mobile/tlm-c${num.nextInt(70)}1024by720.jpg";
  }
}
