import 'package:angular/angular.dart';

@Component(
  selector: 'roger',
  styleUrls: ['roger.css'],
  templateUrl: 'roger.html',
)
class Roger {
  String img = '/static/images/misc/tlm-c19-1920x1200.jpg';
  String footer = "By Go -> ConnectX";
}
